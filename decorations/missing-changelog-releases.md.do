redo-ifchange \
	../sources/combined_release_list.txt \
	rows-with-blank-column

echo "Releases missing a changelog"
echo "----------------------------"
echo ""
echo "We believe the following releases existed because we have an archive,"
echo "but no known changelog for that release has survived."
echo ""
echo "| Date | Version |"
echo "| ---- | ------- |"

./rows-with-blank-column 2 < ../sources/combined_release_list.txt |
	cut -d" " -f1,2 |
	sed -e "s/ / | /; s/^/| /; s/$/ |/"
