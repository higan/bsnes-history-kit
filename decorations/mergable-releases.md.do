redo-ifchange \
	../sources/combined_release_list.txt \
	mergable-releases

echo "Mergable releases"
echo "-----------------"
echo ""
echo "If a release without a changelog comes immediately after"
echo "a release without an archive (or vice versa),"
echo "it's possible that they're really the same release"
echo "and should be merged. Or maybe they're legitimately different."
echo ""
echo "Here are potentially-mergable releases to be investigated:"
echo ""
echo "| Release A | Release B |"
echo "| --------- | --------- |"

./mergable-releases ../sources/combined_release_list.txt |
	sed -e "s/ / | /; s/^/| /; s/$/ |/"
