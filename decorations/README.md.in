bsnes-history
=============

This repository records the historical development of the bsnes emulator,
reconstructed from surviving changelogs and release archive
by [the bsnes history kit][kit].

[kit]: https://gitlab.com/higan/bsnes-history-kit/

This repository is not stable, and may be rebuilt from scratch as the kit is
updated with newly recovered archives, or smarter ways to process the source
material into a Git repository.

For the latest progress of bsnes/higan, see [the unofficial higan
repository][higan].

[higan]: https://gitlab.com/higan/higan/

Repository layout
=================

The `master` branch only contains this README, and can be ignored.

The `history` branch contains the full development history reconstructed by
[the bsnes history kit][kit].

Tags like `v123` exist for each surviving bsnes release.

A special tag, `graft-point`, marks the commit that corresponds to the oldest
commit in [the unofficial higan repository][higan]. It can be used to graft the
reconstructed bsnes history onto the higan repository, like this:

    $ cd path/to/higan/repository
    $ git fetch path/to/this/repository tag graft-point
    $ git replace 165f1e7 graft-point

On version numbering
====================

byuu's version numbering conventions have changed over time,
but in this repository they have generally been normalised
to the "modern" convention:

  - Stable releases have a version number like `v123`,
    where the number increases by one, and is zero-padded to three digits
    (so `v005`, or `v037`)
  - The first work-in-progress release after stable release `v123` is `v123r01`,
    followed by `v123r02`, and so forth.
    
Because software development is not always straight-forward,
there are additional quirks to be aware of:

  - Sometimes, if a stable release had a problem,
    byuu would release a fixed version with an "a" suffix.
  - Sometimes if byuu was part-way through a change
    that was not yet ready for testing,
    he made a relase with a datestamp instead of a regular version number.
  - In the run-up to a new stable release,
    sometimes there are "release candidate" releases,
    like `v064rc1`.
  - The bsnes `v091` release was immediately followed by
    the higan `v091` release,
    which was basically the same but with the name and logo changed.

Because software archaeology is also not straight-forward,
there are even more additional quirks.

  - Sometimes, if a stable release had a problem that was detected very quickly,
    or if a WIP release didn't work out
    and byuu reverted the code base to an earlier release,
    byuu would make a new release re-using the previous version number.
    In these cases,
    I have typically added an "a" or "b" suffix to the later release.
    For example: `v092b`, `v103a`, `v087r03a`.
  - Because WIP releases were not publicly available,
    the archives have not always survived,
    and the changelogs did not always mention the exact version number
    they referred to.
    In these cases, I put a `?` at the end of the version number,
    to signify that it's a guess.

History holes
=============

Because bsnes has been around for a long time,
some history has been lost.
The following tables list some of the missing information.

There are probably also releases for which we have
neither a changelog nor an archive,
but since we have no evidence of them at all,
we can't put them into a list.
