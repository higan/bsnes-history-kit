redo-ifchange \
	../sources/combined_release_list.txt \
	rows-with-blank-column

echo "Releases missing an archive"
echo "---------------------------"
echo ""
echo "We believe the following releases existed because we have a changelog,"
echo "but no known archive of that release has survived."
echo ""
echo "| Date | Version |"
echo "| ---- | ------- |"

./rows-with-blank-column 3 < ../sources/combined_release_list.txt |
	cut -d" " -f1,2 |
	sed -e "s/ / | /; s/^/| /; s/$/ |/"
