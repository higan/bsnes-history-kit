# Already a tar file, doesn't need repacking.
redo-ifchange "$2.tar.bz2"
ln "$2.tar.bz2" "$3"
