Lots of changes this time around. FreeBSD stability and compilation is
still a work in progress.

FreeBSD 10 + Clang 3.3 = 108fps
FreeBSD 10 + GCC 4.7 = 130fps

**Errata 1:** I've been fighting that god-damned endian.h header for
the past nine WIPs now. The above WIP isn't building now because
FreeBSD isn't including headers before using certain types, and you
end up with a trillion error messages. So just delete all the endian.h
includes from nall/intrinsics.hpp to build.

**Errata 2:** I was trying to match g++ and g++47, so I used
$(findstring g++,$(compiler)), which ends up also matching clang++.
Oops. Easy fix, put Clang first and then else if g++ next. Not ideal,
but oh well. All it's doing for now is declaring -fwrapv twice, so you
don't have to fix it just yet. Probably just going to alias
g++="g++47" and do exact matching instead.

**Errata 3:** both OpenGL::term and VideoGLX::term are causing a core
dump on BSD. No idea why. The resources are initialized and valid, but
releasing them crashes the application.

Changelog:
* nall/Makefile is more flexible with overriding $(compiler), so you
can build with GCC or Clang on BSD (defaults to GCC now)
* PLATFORM_X was renamed to PLATFORM_XORG, and it's also declared with
PLATFORM_LINUX or PLATFORM_BSD
** PLATFORM_XORG probably isn't the best name ... still thinking about
what best to call LINUX|BSD|SOLARIS or ^(WINDOWS|MACOSX)
* fixed a few legitimate Clang warning messages in nall
* Compiler::VisualCPP is ugly as hell, renamed to Compiler::CL
* nall/platform includes nall/intrinsics first. Trying to move away
from testing for _WIN32, etc directly in all files. Work in progress.
* nall turns off Clang warnings that I won't "fix", because they
aren't broken. It's much less noisy to compile with warnings on now.
* phoenix gains the ability to set background and foreground colors on
various text container widgets (GTK only for now.)
* rewrote a lot of the MSU1 code to try and simplify it. Really hope I
didn't break anything ... I don't have any MSU1 test ROMs handy
* SNES coprocessor audio is now mixed as sclamp<16>(system_sample +
coprocessor_sample) instead of sclamp<16>((sys + cop) / 2)
** allows for greater chance of aliasing (still low, SNES audio is
quiet), but doesn't cut base system volume in half anymore
* fixed Super Scope and Justifier cursor colors
* use input.xlib instead of input.x ... allows Xlib input driver to be
visible on Linux and BSD once again
* make install and make uninstall must be run as root again; no longer
using install but cp instead for BSD compatibility
* killed $(DESTDIR) ... use make prefix=$DESTDIR$prefix instead
* you can now set text/background colors for the loki console via
(eg):
** settings.terminal.background-color 0x000000
** settings.terminal.foreground-color 0xffffff
