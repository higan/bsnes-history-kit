Fairly major changes here, I'd appreciate some testing if anyone's not
busy. Note that the save state version has been bumped, so consider
WIP saves unstable until v066 official.

Rewrote the entire scheduling system. Instead of having a global class
that all of the chips call into, each class now contains its own
thread, clock and frequency information. They also implement their own
syncing primitives, but with a tiny bit of standardization.
void step(unsigned clocks) -- add to/subtract from counter based on
master/slave select.
void synchronize_chip() -- make sure chip is caught up to our current
thread before continuing.

So we go from scheduler.sync_copcpu() to sa1.synchronize_cpu(); with
the sa1. being omitted inside the SA1 class itself.

The S-CPU implementation also adds an array<Processor*> coprocessors;
list, and iterates them all. This allows bsnes to have an infinite
number of additional coprocessors at the same time. But there is still
the limitation of only one of each type. So you can use the XML memory
mapping to make a cartridge that contains a SuperFX2 chip, an SA-1
chip, an MSU1 chip and that can be debugged via serial communications.
However, you can't make a cart that has two SA-1 chips. That
limitation probably could be overcome as well, but it's less
important. I mainly wanted to be able to use MSU1 and Serial on
special chip games.

I implemented the synchronization primitives in the chip base classes,
which means that for inlining purposes I had to make it all global, so
you'll have src/cpu/synchronization.hpp, etc now. This was to reduce
some redundancy of having the same exact code inside both bPPU and
sPPU, etc. I'll probably make a Coprocessor : Processor class to
automatically implement the step+synchronize_cpu functions for the
five coprocessors next.

The Scheduler class is actually still around, but it's very trivial
now. It simply saves the program thread and last active emulation
thread for the purpose of entering and exiting emulation. Because I
killed Scheduler::init(), which was responsible for destroying and re-
creating threads, I had to create the threads inside
ChipName::create(), which I call at power and reset. This means that
to load a save state, the system needs to be reset to destroy those
threads.

This caused some troubles with the SA-1, specifically in Kirby's
Dreamland 3, but no other games I tried. I had to move the SA-1 bus
initialization to the power-on event, and only reset when loading a
save state. It would appear that the SA-1 is leaking bus mapping state
information, presumably from the SA-1 MMIO registers that control some
of the mapping layout. If I add remapping of those sections into the
SA1::serialize() function, it should take care of that problem and I
can move sa1bus.init() back into SA1::reset().

All of this results in a 2-3% speed hit for normal games, and a 6-7%
speed hit for special chip games. The former should not have any speed
hit, so I will have to look into what's going on there. The latter we
have no choice on, to allow for more than one coprocessor, the
coprocessor synchronization needs to iterate a list, compared to a
single hard-coded check in the previous builds. If I can figure out
what is happening with the regular game speeds, it should drop the
special chip hit to 4%. Worst part is this hit is in addition to the
10-15% speed hit from v065 official where I wasn't syncing the special
chips up to the CPU except once per scanline and on MMIO accesses.

But that's progress. v065 is definitely going to be the preferred
build for playing SuperFX/SA-1 games for a long time to come.
