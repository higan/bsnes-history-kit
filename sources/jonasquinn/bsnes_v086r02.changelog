Fixed Super Game Boy RAM saving and loading. It plainly wasn't hooked
up at all. Was apparently hard-coded before it became a multi-
emulator.
I also fixed a crashing issue when loading Satellaview-slotted or
Satellaview games without specifying the sub-cart, wasn't setting
has_bsx_slot = true, so the raw memory wasn't being allocated
internally when it wasn't mapped in. Of course a better fix would be
to just not physically map the ranges if the things aren't present.
Kind of a lazy hack to map blank cartridges there, but oh well.
Oh, fixed title displays as well; and did the best I could for now
with regards to multi-file path saving.

Basically, Satellaview-slotted and Sufami Turbo modes need special
handling for cheats.xml and states.bsa.
Your cheat codes may apply to where the two different ST games are
mapped in currently, or your save state may be dependent upon the
Tengai Makyou pack being loaded into Same Game. Storing the cheats.xml
in either the base or one of the slot folders isn't enough. The base
can have different slotted carts, and the slotted carts can be put in
different configurations or into different cartridges.

We need something like this:
    Base (US).sfc/
    Slot 1 (US).bs + Slot 2 (US).bs/
    cheats.xml
    state-1.bsa
    Slot 1 (US).st/
    Slot 2 (US).st/


... but I really don't want to jump through a million hoops to make
that happen. So for now, these two modes are rare enough that I'll
just have them store to the first child slot. Use cheats and states on
them at your own risk. Once we enforce folder loading across the
board, I can simplify things and do the above, no problem.

Aside: the complexity really has to stop here. bsnes is starting to
get overwhelming for just me alone. The abstractions needed to support
multiple systems at a time (and I want that very much, fuck having
five different GUIs) is really starting to get hard to keep in order
in my head.
