phoenix has been completely rewritten from scratch, and bsnes/ui +
bsnes/ui-gameboy have been updated to use the new API. Debugger works
too. Currently, only phoenix/Qt is completed, and there are two known
issues:
1: font sizes of menu items are wrong, I can fix this easily enough
2: there's some sort of multi-second lag when loading games, not sure
what's happening there yet

The new phoenix isn't exactly complete yet, still making some key
changes, and then I'll start on phoenix/Windows and phoenix/GTK+.

The most noticeable difference is that you don't have to give all of
the header paths and PHOENIX_PLATFORM defines when compiling
individual GUI object files. It's only needed for phoenix.cpp itself.
The overall structure of the phoenix source folder is much saner as
well for sync.sh.

I'm really surprised things are working as well as they are for a two-
day power rewrite of an entire phoenix target. The other targets won't
be as bad insofar as the core stuff is completed this time. And thank
god for that, I was about ready to kill myself after writing dozens of
lines like this:

    HorizontalSlider::HorizontalSlider() : state(*new State),
    base_from_member<pHorizontalSlider&>(*new
    pHorizontalSlider(*this)),
    Widget(base_from_member<pHorizontalSlider&>::value),
    p(base_from_member<pHorizontalSlider&>::value) {}


But each platform does have some new, unique problems. phoenix/GTK+
was acting screwy prior to the rewrite, and will most likely still
have issues. Even more important, one of the major points of this
rewrite was having the new phoenix/core cache widget settings/data, so
that I can destroy and recreate widgets rather than relying on
SetParent. This means that simple copying of the old phoenix/Windows
won't work, and this new method is significantly more involved.
