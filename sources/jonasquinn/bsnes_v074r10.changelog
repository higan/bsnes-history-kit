**Major** WIP, countless changes. I really went to town on cleaning up
the source today with all kinds of new ideas.
I'll post the ones I remember, use diff -ru to get the rest.

What I like the most is my new within template:
    template<unsigned lo, unsigned hi>
    alwaysinline bool within(unsigned addr) {
    static const unsigned mask = ~(hi ^ lo);
    return (addr & mask) == lo;
    }


Before, you would see code like this:
    if((addr & 0xe0e000) == 0x206000) {  //$20-3f:6000-7fff


The comment is basically necessary, and you have to trust that the
mask is right, or do the math yourself.

Now, it looks like this:
    if(within<0x20, 0x3f, 0x6000, 0x7fff>(addr)) {


That's the same as within<0x206000, 0x3f7fff>, I just made an SNES-
variant to more closely simulate my XML mapping style:
20-3f:6000-7fff.

Now obviously this has limitations, it only works in base-2 and it
can't manage some tricky edge cases like (addr & 0x408000) == 0x008000
for 00-3f|80-bf:8000-ffff. But for the most part, I'll be using this
where I can. The Game Boy is fully ported over to it (via the MBCs),
but the SNES only has the BS-X town cartridge moved over so far.
SuperFX and SA-1 at the very least could benefit.

Next up, since the memory map is now static, there's really no reason
to remap the entire thing at power-on _and_ reset. So it is now set up
at cartridge load and that's it. I moved the CPU/PPU/WRAM mapping out
of memory.cpp and into their respective processors. A bit of
duplication only because there are multiple processor cores for the
different profiles, but I'm not worried about that. This is also going
to be necessary to fix the debugger.

Next, Coprocessor::enable() actually does what I initially intended it
to now: it is called once to turn a chip on after cartridge load. It's
not called on power cycle anymore. This should help fix power-cycle on
my serial simulation code, and was needed to map the bus exactly one
time. Although most stuff is mapped through XML, some chips still need
some manual hooks for monitoring and such (eg S-DD1.)

Next, I've started killing off memory::, it was initially an over-
reaction to the question of where to put APURAM (in the SMP or DSP?)
The idea was to have this namespace that contained all memory for
everything. But it was very annoying and tedious, and various chips
ignored the convention anyway like ST-0011 RAM, which couldn't work
anyway since it is natively uint16 and not uint8. Cx4 will need 24-bit
RAM eventually, too. There's 8->24-bit functions in there now, because
the HLE code is hideous.

So far, all the cartridge.cpp memory:: types have been destroyed.
memory::cartrom, memory::cartram become cartridge.rom and
cartridge.ram. memory::cartrtc was moved into the SRTC and SPC7110
classes directly. memory::bsxflash was moved into BSXFlash.
memory::bsxram and memory::bsxpram were moved into BSXCartridge (the
town cartridge). memory::st[AB](rom|ram) were moved into a new area,
snes/chip/sufamiturbo. The snes/chip moniker really doesn't work so
well, since it also has base units, and the serial communications
stuff which is through the controller port, but oh well, now it also
has the base structure for the Sufami Turbo cartridge too. So now we
have sufamiturbo.slotA.rom, sufamiturbo.slotB.ram, etc.

Next, the ST-0010/ST-0011 actually save the data RAM to disk. This
wasn't at all compatible with my old system, and I didn't want to keep
adding memory types to check inside the main UI cartridge RAM loading
and saving routines.

So I built a NonVolatileRAM vector inside SNES::Cartridge, and any
chip that has memory it wants to save and load from disk can append
onto it : data, size, id ("srm", "rtc", "nec", etc) and slot (0 =
cartridge, 1 = slot A, 2 = slot B)

To load and save memory, we just do a simple: foreach(memory,
SNES::cartridge.nvram) load/saveMemory(memory).

As a result, you can now keep your save games in F1 Race of Champions
II and Hayazashi Nidan Morita Shougi.
Technically I think Metal Combat should work this way as well, having
the RAM being part of the chip itself, but for now that chip just
writes directly into cartridge.ram, so it also technically saves to
disk for now.

To avoid a potential conflict with a manipulated memory map, BS-X SRAM
and PSRAM are now .bss and .bsp, and not .srm and .psr.
Honestly I don't like .srm as an extension either, but it doesn't
bother me enough to break save RAM compatibility with other emulators,
so don't worry about that changing.

I finally killed off MappedRAM initializing size to ~0 (-1U). A size
of zero means there is no memory there just the same. This was an old
holdover for handling MMIO mapping, if I recall correctly. Something
about a size of zero on MMIO-Memory objects causing it to wrap the
address, so ~0 would let it map direct addresses ... or something.
Whatever, that's not needed at all anymore.

BSXBase becomes BSXSatellaview, and I've defaulted the device to being
attached since it won't affect non-BSX games anyway. Eventually the
GUI needs to make that an option. BSXCart becomes BSXCartridge.
BSXFlash remains unchanged.

I probably need to make Coprocessor::disable() functions now to free
up memory on unload, but it shouldn't hurt anything the way it is.

libsnes is most definitely broken to all hell and back now, and the
debugger is still shot. I suppose we'll need some tricky code to work
with the old ID system, and we'll need to add some more IDs for the
new memory types.
