redo-ifchange "$2.rar"

extract_path="$2.rar.files"

rm -rf "$extract_path"
mkdir -p "$extract_path"

unrar x "$2.rar" "$extract_path" >&2

( cd "$extract_path"; 7z a -ttar -so "$2.tar" | 7z a -tgzip -si -so "$2.tar.gz" ) > "$3"

rm -rf "$extract_path"
