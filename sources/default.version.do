case "$2" in
	# After v089r05, byuu released v089r06 and v089r07 with various
	# changes, but ultimately decided to revert to v089r05 and try
	# a different approach... resulting in new and different
	# releases also called v089r06 and v089r07. By comparing the
	# contents of the tarballs in the tukuyomi archive with the
	# corresponding revisions of the unofficial higan repo, we
	# discovered that the tukuyomi archive's copies of those WIPs
	# happen to be the later versions, so we'll add an "a" suffix to
	# disambiguate.
	tukuyomi/*/bsnes_v089r06.*)
		echo "Adjusting re-released v089r06 in $2" >&2
		echo "bsnes_v089r06a"
		;;
	tukuyomi/*/bsnes_v089r07.*)
		echo "Adjusting re-released v089r07 in $2" >&2
		echo "bsnes_v089r07a"
		;;

	# Much like the previous case, after v087r02 byuu released
	# v087r03 and r04, then reverted back to r02 and released
	# another, different r03 and r04. This time, it seems the
	# tukuyomi archive contains the older r03 but the later r04.
	tukuyomi/*/bsnes_v087r04.*)
		echo "Adjusting re-released v087r04 in $2" >&2
		echo "bsnes_v087r04a"
		;;

	# Cydrak modified v072r10 to improve the uPD77C25 emulation,
	# and uploaded it with a ".01c" suffix. Because we only care
	# about byuu's code, we'll ignore this archive.
	tukuyomi/*/bsnes_v072r10.01c*)
		echo "Ignoring Cydrak's release $2" >&2
		;;

	# Some archives are corrupt and can't be automatically
	# extracted. We keep them around in case somebody wants to try
	# and reconstruct them in the future.
	tukuyomi/*/bsnes_v055.zip)
		echo "Ignoring corrupt file $2" >&2
		;;
	tukuyomi/*/bsnes_v073.tar.bz2)
		echo "Ignoring corrupt file $2" >&2
		;;

	# Sometimes byuu published the straight source-code in
	# a tarball, and the source-code bundled with pre-built binaries
	# in a .7z file. Since we're only interested in the source,
	# we'll ignore those .7z files here.
	screwtape/higan_v091r15.7z|\
	screwtape/higan_v095r01.7z|\
	screwtape/higan_v102-source.7z)
		echo "Ignoring redundant archive $2" >&2
		;;

	# Most archives are like "bsnes_v123r45.tar.bz2"
	# Because the tukuyomi and screwtape collection have some
	# overlap, we'll only take higan archives from the screwtape
	# collection, and bsnes archives from everywhere else.
	*bsnes_v*|screwtape/higan_v*)
		echo "$2" |
		grep -o '\(bsnes_v\|higan_v\)[0-9][A-Za-z0-9_]*\(\.01c\)\?' |
		sed \
			-e 's/_wip\([0-9][0-9]\)/r\1/g' \
			-e 's/_wip\([0-9]\)/r0\1/g' \
			-e 's/_src//'
		;;
	tukuyomi/*/higan_v*)
		echo "Ignoring tukuyomi's higan archive $2 " >&2
		;;

	# Between v067r05 and v067r21, byuu introduced the accuracy core, and
	# wasn't sure if "bsnes" would refer to both cores or just the original,
	# so there were some releases named like "snes-20100808.tar.bz2".
	# However, we don't want to pick up the unrelated bsnes-qt release.
	*snes-2*)
		echo "$2" | grep -o 'b\?snes-[-0-9]*'
		;;

	# At various times, byuu has made experimental higan WIPs with
	# datestamps instead of version numbers.
	*higan_2*)
		echo "$2" | grep -o 'higan_[-0-9]*'
		;;

	# We don't care about all the other bits and bobs.
	*)
		echo "Unrecognised bsnes version in $2" >&2
		;;
esac
