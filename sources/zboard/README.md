Source material from the ZSNES board
====================================

Between about v030 and v042,
bsnes development discussion occurred in
the "bsnes Dev Talk" forum
on the ZSNES forums at https://board.zsnes.com/

The files in the [board.zsnes.com](./board.zsnes.com/) directory
are a record of the text of all the posts in the bsnes Dev Talk forum
as of 2019-03-18.
The forum in question was locked in 2009
when byuu set up his first official forums,
so it should still be a good record of the original content.

The file [changelog_posts.txt](./changelog_posts.txt)
contains identifiers for posts that seemed to be WIP changelogs,
and the (possible) version strings associated with them.

  - The [tukuyomi collection](../tukuyomi)
    already provides changelogs for each release from v030 to v042,
    so I was only interested in WIP releases.
  - To generate the list,
    I used the [extract-posts](./extract-posts) script
    to break each post into a separate file,
    the [html-to-text](../html-to-text) script
    to convert each post into easily searchable text,
    and searched for posts from byuu that mentioned "wip".
  - Some WIP release announcements explicitly mention the WIP version,
    so those are presumably reliable.
  - Version numbers ending with a question-mark
    (like `bsnes_v030r04?`)
    are guesses, since the changelog didn't mention an exact version.
  - Some guesses should be pretty accurate; for example, the changelog for
    `bsnes_v039r20` mentions "changes from wip19",
    and there are exactly 19 WIP announcements in the thread before it,
    so they should be correctly numbered.
  - Some guesses are wilder; for example,
    the changelog for `bsnes_v038r08?` has a reference to "07",
    but there were only five WIP announcements in the thread before it.
