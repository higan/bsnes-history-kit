redo-ifchange \
	board.zsnes.com/phpBB3/viewtopic.php* \
	extract-posts \
	changelog_posts.txt

rm -rf posts
./extract-posts board.zsnes.com/phpBB3/viewtopic.php* posts

cat changelog_posts.txt | while read version path; do
	../html-to-text "$path/body.xml" > "$version.changelog"
	printf "%s %s %s\n" \
		"$(cat "$path/date")" \
		"$version" \
		"$version.changelog"
done
