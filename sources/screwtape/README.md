Source material from screwtape
==============================

All the archives in this directory were collected by myself, while working on
the unofficial higan repository.

Note that some files have been renamed from their original names:

  - `higan_v098a-source.7z`
  - `higan_v100a-source.7z`
  - `higan_v103a-source.7z`
  - `higan_v104a-source.7z`

These files originally did not have the "a" after the version number,
but their content differs from other archives with the same version number,
so they were given new names to keep them distinct.

**NOTE:** Although bsnes v107 came after higan v106r107, it was actually forked
from v106r45, avoiding most of the breaking changes from r46 onward.
