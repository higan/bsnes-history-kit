redo-always

printf "%s\n" \
	tukuyomi/emus/bsnes/*.rar \
	tukuyomi/emus/bsnes/*.zip \
	tukuyomi/emus/bsnes/*.tar.bz2 \
	tukuyomi/emus/bsnes/*.tar.xz \
	byuu/*.tar.bz2 \
	screwtape/*.7z \
	screwtape/*.tar.xz |
sort > "$3"

redo-stamp < "$3"
