redo-ifchange "$2"

case "$2" in
	*.zip|*.rar|*.7z)
		7z l "$2"
		;;
	*.tar.*)
		7z e "$2" -so | 7z l -ttar -si
		;;
esac
