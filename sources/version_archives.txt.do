redo-ifchange all_archives.txt

redo-ifchange $(cat all_archives.txt | sed -e 's/$/.version/')

cat all_archives.txt | while read archive; do
	if [ -f "$archive.version" ]; then
		printf "%s %s\n" "$(cat "$archive.version")" "$archive"
	fi
done
