Source material from byuu
=========================

The files in this directory (with the exception of this README and the
`changelog_list.txt` file) were provided personally by byuu, the creator of the
bsnes/higan project.
