redo-ifchange \
	tukuyomi/changelog_list.txt \
	zboard/changelog_list.txt \
	jonasquinn/changelog_list.txt \
	combine-changelog-lists

./combine-changelog-lists > "$3"

# Let's verify we don't have any duplicate versions in our archives.
cut -d" " -f2 "$3" | sort | uniq -d | (
	duplicates=0
	while read -r version; do
		echo "Multiple changelogs for version $version" >&2
		duplicates=1
	done
	exit "$duplicates"
)
