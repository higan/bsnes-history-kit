redo-ifchange \
	misc/bsnes_changelog.txt \
	misc/bsnes_v045_wip09.html \
	misc/higan_changelog.txt \
	thread_changelog_list.txt \
	extract-changelogs \
	../html-to-text

# The HTML file includes both the public announcement, and the more detailed
# WIP-forum post. We only care about the forum post, so we grab the last 52
# lines of output (which should be "Sorry, but I've given up...".
../html-to-text misc/bsnes_v045_wip09.html |
	sed -n -e '/^Sorry, but/,$p' > bsnes_v045r09.changelog

bsnes_changelog=$(mktemp)
higan_changelog=$(mktemp)
extra_changelog=$(mktemp)

./extract-changelogs misc/bsnes_changelog.txt > "$bsnes_changelog"
# This timestamp comes from the source HTML, manually copied and pasted.
echo "2009-04-30T00:00:00+00:00 bsnes_v045r09 bsnes_v045r09.changelog" > "$extra_changelog"
./extract-changelogs misc/higan_changelog.txt > "$higan_changelog"

# These changelogs aren't exactly sorted, but they're close enough that
# "sort -m" will merge them correctly, preserving their not-quite-sortedness.
sort -m \
	"$bsnes_changelog" \
	"$extra_changelog" \
	"$higan_changelog" \
	thread_changelog_list.txt
