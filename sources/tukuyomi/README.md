Source material from tukuyomi
=============================

The files in subdirectories below this one
came from the tukuyomi SNES emulation archive,
which was once at http://black-ship.net/~tukuyomi/snesemu/
but is now on the Internet Archive:
https://archive.org/details/tukuyomi-snes-archive

For example, the file [misc/bsnes_changelog.txt](misc/bsnes_changelog.txt)
was originally http://black-ship.net/~tukuyomi/snesemu/misc/bsnes_changelog.txt

Files directly inside this directory
(this README, `.gitignore`, `*.do`, etc.)
are not from the tukuyomi archive.

The file [changelog_posts.txt](./changelog_posts.txt)
contains identifiers for posts in
[the bsnes thread archive](misc/bsnes_thread.zip])
that seemed to be WIP changelogs,
and the (possible) version strings associated with them.

  - The main bsnes changelog
    already provides changelogs for each stable releases,
    so I was only interested in WIP releases.
  - To generate the list,
    I used the [extract-posts](./extract-posts) script
    to break each post into a separate file,
    the [html-to-text](../html-to-text) script
    to convert each post into easily searchable text,
    and searched for posts from byuu that mentioned "wip".
  - Some WIP release announcements explicitly mention the WIP version,
    so those are presumably reliable.
  - Version numbers ending with a question-mark
    (like `bsnes_v030r04?`)
    are guesses, since the changelog didn't mention an exact version.
  - Guessed version numbers have been cross-checked
    against stable release dates,
    so those should be accurate.
  - However, not every WIP was released,
    and not every released WIP got announced,
    so we may never know accurate version numbers.
