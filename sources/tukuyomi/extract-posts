#!/usr/bin/env python3
import datetime
import html.parser
import os
import os.path
import sys
import urllib.parse
import xml.etree.ElementTree as ET


class HTMLParser(html.parser.HTMLParser):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__builder = ET.TreeBuilder()

    def close(self):
        super().close()
        return self.__builder.close()

    def handle_starttag(self, tag, attrs):
        self.__builder.start(tag, dict(attrs))

        if tag in {"meta", "br", "hr", "img"}:
            self.__builder.end(tag)

    def handle_endtag(self, tag):
        self.__builder.end(tag)

    def handle_data(self, data):
        self.__builder.data(data)


def iter_posts(topic_page):
    page_body = topic_page.find("body")
    post_table = page_body.find("table[2]/tbody")

    rows = iter(post_table.findall("./tr"))

    index = 0

    while True:
        try:
            row = next(rows)
        except StopIteration:
            break

        author = row.find("td[1]/span[@class='name']/strong")
        details = row.find("td[2]/table/tbody/tr[1]/td/span")
        body = row.find("td[2]/table/tbody/tr[3]/td")

        if author is None or details is None or body is None:
            continue

        # Post details text looks like:
        #
        #    Posted: Wed Aug 31, 2005 4:19 am Post subject: blah
        date = datetime.datetime.strptime(
            " ".join(details.text.split()[1:7]),
            "%a %b %d, %Y %I:%M %p",
        ).replace(tzinfo=datetime.timezone.utc)

        yield (index, author.text, date, body)

        index += 1


def main(args):
    topic_file = args[1]
    destination = args[2]

    with open(topic_file) as handle:
        parser = HTMLParser()
        parser.feed(handle.read())
        tree = parser.close()

    for (post_id, author, date, body) in iter_posts(tree):
        post_path = os.path.join(
            destination,
            "%04d" % post_id,
            urllib.parse.quote(author, safe=""),
        )
        os.makedirs(post_path, exist_ok=True)
        with open(os.path.join(post_path, "date"), "w") as handle:
            handle.write(date.isoformat())

        with open(os.path.join(post_path, "body.xml"), "w") as handle:
            handle.write(ET.tostring(body, encoding="unicode"))


if __name__ == "__main__":
    sys.exit(main(sys.argv))
