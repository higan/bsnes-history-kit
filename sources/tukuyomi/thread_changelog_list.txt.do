redo-ifchange \
	misc/bsnes_thread.zip \
	extract-posts \
	changelog_posts.txt

rm -f bsnes_thread.html
7z e misc/bsnes_thread.zip bsnes_thread.html >&2

rm -rf posts

./extract-posts bsnes_thread.html posts

cat changelog_posts.txt | while read version path; do
	../html-to-text "$path/body.xml" > "$version.changelog"
	printf "%s %s %s\n" \
		"$(cat "$path/date")" \
		"$version" \
		"$version.changelog"
done
