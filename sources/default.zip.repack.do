redo-ifchange "$2.zip"

extract_path="$2.zip.files"

rm -rf "$extract_path"
mkdir -p "$extract_path"

unzip -q -d "$extract_path" "$2.zip" >&2

( cd "$extract_path"; 7z a -ttar -so "$2.tar" | 7z a -tgzip -si -so "$2.tar.gz" ) > "$3"

rm -rf "$extract_path"
