redo-ifchange version_archives.txt

redo-ifchange $(cut -d" " -f2 version_archives.txt | sed -e 's/$/.manifest/')

cat version_archives.txt | while read version archive; do
	if grep -E -l "Makefile$|GNUmakefile$" "$archive.manifest" >/dev/null 2>&1 ; then
		date=$(tail -1 "$archive.manifest" |
			cut -d" " -f1-2 |
			sed -e 's/ /T/' -e "s/$/+00:00/")
		printf "%s %s %s\n" "$date" "$version" "$archive"
	fi
done | sort > "$3"

# Let's verify we don't have any duplicate versions in our archives.
cut -d" " -f2 "$3" | sort | uniq -d | (
	duplicates=0
	while read -r version ; do
		echo "Multiple archives for version $version" >&2
		duplicates=1
	done
	exit "$duplicates"
)
