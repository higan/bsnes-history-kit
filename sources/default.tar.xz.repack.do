# Already a tar file, doesn't need repacking.
redo-ifchange "$2.tar.xz"
ln "$2.tar.xz" "$3"
