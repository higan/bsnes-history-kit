bsnes history kit
=================

The [unofficial higan repository][uhr] records the development of the higan
emulation suite (previously known as bsnes) since late 2010. However, bsnes
was in development for years before that, and although many source archives and
release announcements have been preserved, it's much more difficult and tedious
to trace bsnes' development before the Git repository started than after it.

This repository contains the surviving release archives and changelogs,
scripts to analyse and clean up those sources, and a script to export them
in the format accepted by the `git fast-import` command, to turn them into a
consistently-formatted Git repository.

[uhr]: https://gitlab.com/higan/higan/

Results
=======

If you can't use this kit yourself, or if you'd rather not, an example history
repository is available at https://gitlab.com/higan/bsnes-history/

Note that the pre-built repository may have been built by an older version of
the kit, and so may not be as accurate as it could be.

Ingredients
===========

To build a `bsnes-history` repository, you will need:

  - a POSIX operating system
  - The `7z` archive tool, including the RAR plugin.
  - Python 3.x (tested with 3.7, older versions may work)
  - The content of this repository

Optional ingredients
--------------------

These extra ingredients may improve your experience:

  - [redo] will let you incrementally rebuild the analysis of the sources,
    if you want to tinker with the import process yourself, instead of always
    redoing all the analysis from scratch every time.

[redo]: https://github.com/apenwarr/redo

Building
========

How to build your own `bsnes-history` repository:

 1. Check out this repository somewhere:

        $ git clone https://gitlab.com/higan/bsnes-history-kit.git
        $ cd bsnes-history-kit

 1. Run the preparation script, to analyse and collate the sources:

        $ ./do prepare

    (if you have [redo] installed, you may `redo prepare`, or even
    `redo -j10 prepare` if you have a lot of CPUs and want to save some time)

 1. Make a new Git repository to receive the bsnes history:

        $ cd ..
        $ git init bsnes-history
        $ cd bsnes-history

 1. Export the bsnes historical data into git:

        $ ../bsnes-history-kit/bsnes-fast-export | git fast-import

 1. Because `git fast-import` stores data uncompressed for speed, and because
    it only updates the Git object store, not the working directory, do some
    house-keeping:

        $ git gc --aggressive
        $ git reset --hard HEAD

 1. Enjoy your new bsnes history Git repository!

Grafting
========

The standard bsnes-history repository contains information all the way up to
higan v093r10, but the unofficial higan repo is probably more reliable for the
span of time it covers. Luckily, the `bsnes-history` repository includes
a special `graft-point` tag pointing at the commit corresponding to the
starting point of the unofficial higan repo, so we can use `git replace` to
link them together into one long chain.

Follow the regular building steps above to create a `bsnes-history` repository.
Then:

 1. Check out the unofficial higan repo (if you don't already have one):

        $ cd ..
        $ git clone https://gitlab.com/higan/higan.git
        $ cd higan

 1. Fetch the `graft-point` tag from the bsnes-history repo, which automatically
    fetches all the tags and history leading up to that point:

        $ git fetch ../bsnes-history tag graft-point

 1. Tell git to replace the oldest commit in the higan repo with the newest
    commit on the 'history' branch:

        $ git replace 165f1e7 graft-point

Now commands like `git log` should show you the complete bsnes development
history from the oldest surviving changes up until the present day!

Citations
=========

See the READMEs in each subdirectory of [sources](sources/) for provenance
information..
