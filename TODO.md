- Figure out how many leading directories to strip from each archive
- Normalise whether the bsnes code lives at the root, or inside a 'bsnes'
  directory beside other things like 'snesreader' and 'snesfilter.
    - generally, releases have everything, and WIPs only include the bsnes code.
- Check the various pairs listed in the Mergable Releases report.
- Teach the zboard changelog source to extract the relevant parts of each post,
  rather than dumping the entire thing.
- Double-check [Google Code](https://code.google.com/archive/p/bsnes/downloads)
  to see if there's any source or binary archives missing from tukuyomi's
  collection.
    - It's a pain to bulk-download from Google Code, but Alcaro has made a
      mirror on GitHub: https://github.com/Alcaro/bsnes-gc
    - It definitely includes a good v073 source archive
- There's a bunch of changelog-type-text on byuu's old site:
  https://web.archive.org/web/20070501000000*/http://byuu.cinnamonpirate.com/?page=bsnes_news
  Sometimes it mentions WIPs,
  sometimes it has screenshots with version numbers,
  which would help pin down the dates of some changes
  that occurred before byuu started doing most things on forums.
    - Apparently you can get the original scraped HTML
      by replacing the `*` in Wayback Machine URL with `id_`,
      and get a list of scrapes from, say,
      `https://web.archive.org/web/*/byuu.cinnamonpirate.com/*`
