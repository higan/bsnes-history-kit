redo-ifchange \
	sources/archive_release_list.txt \
	sources/combined_release_list.txt \
	decorations/README.md

redo-ifchange $(cut -d" " -f3 sources/archive_release_list.txt | sed -e 's|.*|sources/&.repack|')
